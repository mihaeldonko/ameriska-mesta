
@include('head')
<body onload="header_diff();loadImg();jezik_save();local_test()">
@include('header')


  <div
    class="p-5 text-center bg-image"
    style="
      background-image: url('https://cdn.curiositystream.com/system/Playlist/images/000/000/139/hero/AmericanHistory-1440x550.jpg');
      height: 400px;
    "
  >

    <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
      <div class="d-flex justify-content-center align-items-center h-100 mt-5">
        <div class="text-white">
          <h1 class="mt-5" id="big_city">Biggest cities</h1>
          <h4 class="mb-5" id="where">in USA</h4>
        </div>
      </div>
    </div>
  </div>
</header>
    <div class="container-fluid mt-5">
        <div class="row">
          <div class="col-md-2"></div>
        <div class="col-md-8 bg-dark"> 
          <h1 class="mt-3 mb-2 text-light" id="filter">Filter:</h1>
            <x-alert></x-alert>

          <label for="myInput" class="text-light" id="select">Select state</label>
          <select class="form-control form-select" id="ddlViewBy">

          <option value="none"></option>
            <x-usa_list></x-usa_list>
          </select>
          <button class="btn btn-success mt-3 mb-3" onclick="sort_by_state()" id="search">Search by state</button>
          

          </div>
        </div>
        
            <div class="col-md-8 offset-md-2 bg-dark">
            <h1 class="mt-1 mb-5 text-light" id="idk">List of biggest cities in USA</h1>
            </div>   
        </div>
    <div class="container" id="test">
        <div class="row text-center">
        @foreach($cities as $key=>$city)
        <div class="col-md-4 " id="id{{$key}}">
        <img height="250" width="250" src="" class=" mt-3" alt="" id="img{{$key}}">
      <h4 class="text-center" ><a href="http://localhost:8000/mesto/{{$city->ID}}" id="name{{$key}}">{{$city->city}}</a></h4>
      <h6 class="text-center" id="rank{{$key}}">Rank: {{$city->rank}}</h6>
      <h6 class="text-center" id="population{{$key}}"><span id="population">Population:</span> {{$city->population}}</h6>
      <h6 class="text-center" id="state{{$key}}">{{$city->state}}</h6>
        </div>
        <p hidden>{{ ++$key }}</p>
        @endforeach
            <p id="count" hidden>{{$key}}</p>
        </div>
    </div>

    
    @include('footer')

<script>
function filter_kljucna_beseda(){

let input, filter, table, tr, td, txtValue;

input = document.getElementById("myInput");
filter = input.value.toUpperCase();

var state = document.getElementById("ddlViewBy").value;

var count = document.getElementById("count").innerHTML;

if(state === "none"){
  for (let i = 0; i < count; i++) {
  test = document.getElementById(`state${i}`).innerHTML;
    td1 = document.getElementById(`name${i}`);
    if (td1) {
        txtValue =td1.textContent || td1.innerText ;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {

            document.getElementById(`id${i}`).style.display = "";

            
        } else {
          document.getElementById(`id${i}`).style.display = "none";
        }
    }
}
}else{
for (let i = 0; i < count; i++) {
  test = document.getElementById(`state${i}`).innerHTML;
    td1 = document.getElementById(`name${i}`);
    if (td1) {
        txtValue =td1.textContent || td1.innerText ;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          if(state===test){
            document.getElementById(`id${i}`).style.display = "";
          }
            
        } else {
          document.getElementById(`id${i}`).style.display = "none";
        }
    }
}
}
}
function local_test(){
  if(localStorage.getItem("jezik") === "Slovenija"){
    document.getElementById("home").innerHTML = "Domov";
    document.getElementById("add_city").innerHTML = "Dodaj mesta";
    document.getElementById("edit_city").innerHTML = "Spremeni/Izbriši mesto";
    document.getElementById("register").innerHTML = "Registracija";

    document.getElementById("big_city").innerHTML = "Največja mest";
    document.getElementById("where").innerHTML = "v ZDA";
    document.getElementById("select").innerHTML = "Izberi zvezdno državo";
    document.getElementById("search").innerHTML = "Išči po zvezdni državi";
    document.getElementById("idk").innerHTML = "Lista največjih mest v ZDA";
    document.getElementById("city_name").innerHTML = "Išči po imenu mesta";
  }else{
    document.getElementById("home").innerHTML = "Home";
    document.getElementById("add_city").innerHTML = "Add city";
    document.getElementById("edit_city").innerHTML = "Edit/Delete City";
    document.getElementById("register").innerHTML = "Registration";

    document.getElementById("big_city").innerHTML = "Biggest cities";
    document.getElementById("where").innerHTML = "in USA";
    document.getElementById("select").innerHTML = "Select by state";
    document.getElementById("search").innerHTML = "Search by state";
    document.getElementById("idk").innerHTML = "List of biggest countries in usa";
    document.getElementById("city_name").innerHTML = "Search by city name";
  }
}
function test(){
  if(document.getElementById('togBtn').checked){
    document.getElementById("home").innerHTML = "Domov";
    document.getElementById("add_city").innerHTML = "Dodaj mesta";
    document.getElementById("edit_city").innerHTML = "Spremeni/Izbriši mesto";
    document.getElementById("register").innerHTML = "Registracija";

    document.getElementById("big_city").innerHTML = "Največja mest";
    document.getElementById("where").innerHTML = "v ZDA";
    document.getElementById("select").innerHTML = "Izberi zvezdno državo";
    document.getElementById("search").innerHTML = "Išči po zvezdni državi";
    document.getElementById("idk").innerHTML = "Lista največjih mest v ZDA";
    document.getElementById("city_name").innerHTML = "Išči po imenu mesta";
  }else{
    document.getElementById("home").innerHTML = "Home";
    document.getElementById("add_city").innerHTML = "Add city";
    document.getElementById("edit_city").innerHTML = "Edit/Delete City";
    document.getElementById("register").innerHTML = "Registration";

    document.getElementById("big_city").innerHTML = "Biggest cities";
    document.getElementById("where").innerHTML = "in USA";
    document.getElementById("select").innerHTML = "Select by state";
    document.getElementById("search").innerHTML = "Search by state";
    document.getElementById("idk").innerHTML = "List of biggest countries in usa";
    document.getElementById("city_name").innerHTML = "Search by city name";
  }
}

function sort_by_state(){

  
 var state = document.getElementById("ddlViewBy").value;
 
 var count = document.getElementById("count").innerHTML;

if(state === "none"){
  location.reload();
}else{
  for(let x = 0;x<count; x++){
  document.getElementById(`id${x}`).style.display = 'block';
 }
 
 for(let i = 0;i<count; i++){
  var temp = document.getElementById(`state${i}`).innerHTML;

  if(temp !== state){
    document.getElementById(`id${i}`).style.display = 'none';
  }
 }
 document.getElementById("myInput").value = "";
}

}

</script>
<script>  
    function loadImg() {

   
      const key = "28248024-1b0004ace62f794d221e735d1";
      var php_var = "<?php echo $key; ?>";

   for(let i = 0;i<php_var;i++){
    const search = document.getElementById(`name${i}`).innerHTML;
    const url = `https://pixabay.com/api/?key=${key}&q=${search}&image_type=photo`;
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
          try{
            document.getElementById(`img${i}`).src = data.hits[0].webformatURL;
          }catch(error){
            document.getElementById(`img${i}`).src = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/USA_Flag_Map.svg/2560px-USA_Flag_Map.svg.png";
          }

            
            });
    
   }

}
</script>

 
</body>
</html>


