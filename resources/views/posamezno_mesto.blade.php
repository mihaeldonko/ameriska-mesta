
@include('head')
<body onload="slike();header_diff()">
@include('header')
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light">{{$mesta->city}}</h4>

              <h2 id="ime_states" class="mb-5  text-light">{{$mesta->state}}</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="row mt-3">
            <class class="col-md-3 mb-3">
                <h3>Osnovni Podatki</h3><br>
                <h4>City Name:</h4><h5 id="mesta">{{$mesta->city}}</h5><hr>
                <h4>Name of the state:</h4><h5 id="Zvezdne">{{$mesta->state}}</h5><hr>
                <h4>Population:</h4><h5 id="Populacija">{{$mesta->population}}</h5><hr>
                <h4>Population Growth:</h4><h5 id="Rast">{{$mesta->growth_from_2000_to_2013}}</h5><hr>
                <h4>Rank:</h4><h5 id="rank">{{$mesta->rank}}</h5><hr>
                <h4>latitude:</h4><h5 id="latitude">{{$mesta->latitude}}</h5><hr>
                <h4>longitude:</h4><h5 id="longitude">{{$mesta->longitude}}</h5>
            </class>
            <class class="col-md-6 mb-3">
                <div id="Map" style="height:450px"></div>
    </class>
    
            <class class="col-md-3 mb-3">
                <h3>Weather</h3><br>
                <h4>Temperature:</h4><h5 id="temp">none</h5> <img src="" class="img-fluid" height="50" width="50"  id="icon"><hr>
                <h4>Humidity:</h4><h5 id="soparnost">none</h5><hr>
                <h4>Wind speed:</h4><h5 id="hitrost">none</h5><hr>
                <h4>Wind direction:</h4><h5 id="smer">none</h5><hr>
                <h4>Air pressure:</h4><h5 id="tlak">none</h5><hr>
                <h4>UV index:</h4><h5 id="uv">none</h5><hr>
                <h4>Local time:</h4><h5 id="lokalni_cas">none</h5>

                </class>
                <hr>
        </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <h2 class="text-center mb-5 mt-5">Galery:</h2>
        </div>
        <hr>
        <div class="col-md-10 offset-md-1 mt-5">
            <img src="" class="img-fluid" height="500" width="500" id="slika1">
            <img src=""  class="img-fluid" height="500" width="500" id="slika2">
            <img src=""  class="img-fluid" height="500" width="500" id="slika3">
            <img src=""  class="img-fluid" height="500" width="500" id="slika4">
            <img src=""  class="img-fluid" height="500" width="500" id="slika5">
            <img src=""  class="img-fluid" height="500" width="500" id="slika6">
            <img src=""  class="img-fluid" height="500" width="500" id="slika7">
            <img src=""  class="img-fluid" height="500" width="500" id="slika8">
            <img src=""  class="img-fluid" height="500" width="500" id="slika9">
            <img src=""  class="img-fluid" height="500" width="500" id="slika10">
        </div>
      </div>
    </div>
    
</body>
<script src="/OpenLayers.js"></script>
<script>
  function header_diff(){
    var userCheck= " <?php echo Auth::check();?>";
    if(userCheck == 1){
      document.getElementById("head_login").style.display = 'none'; 
    }else{
      document.getElementById("head_logout").style.display = 'none'; 
      document.getElementById("head_city").style.display = 'none'; 
    }
  }
</script>

<script>
    var mesto = document.getElementById("mesta").innerHTML;
    var lat = document.getElementById("latitude").innerHTML;
    var lon = document.getElementById("longitude").innerHTML;
    var zoom = 12;

    var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
    var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
    var position       = new OpenLayers.LonLat(lon, lat).transform( fromProjection, toProjection);

    map = new OpenLayers.Map("Map");
    var mapnik         = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);
    markers.addMarker(new OpenLayers.Marker(position));

    map.setCenter(position, zoom);


    fetch(`http://api.weatherapi.com/v1/current.json?key=c431662b20b34ac7b46105935222206&q=${mesto}&aqi=no`)
  .then(response_vreme => response_vreme.json())
  .then(vreme => {
    let smer;
        document.getElementById("icon").src = vreme.current.condition.icon;
        document.getElementById("lokalni_cas").innerHTML = vreme.location.localtime;
        document.getElementById("temp").innerHTML = vreme.current.temp_c+"°C/"+vreme.current.temp_f+"F";
        document.getElementById("soparnost").innerHTML = vreme.current.humidity+"%";
        document.getElementById("uv").innerHTML = vreme.current.uv;
        document.getElementById("smer").innerHTML = vreme.current.wind_dir;
        document.getElementById("tlak").innerHTML = vreme.current.pressure_mb+"BAR";
        document.getElementById("hitrost").innerHTML = vreme.current.wind_kph+"km/h";
  });




</script>
<script>
  function slike(){
    var ime = document.getElementById("mesta").innerHTML;
    const options = {
	method: 'GET',
	headers: {
		'X-RapidAPI-Key': '5a7f0eb464mshce2e8ff49815711p1beb57jsn34a809b609d4',
		'X-RapidAPI-Host': 'google-image-search1.p.rapidapi.com'
	}
};

fetch(`https://google-image-search1.p.rapidapi.com/v2/?q=${ime}_city&hl=en`, options)
	.then(response => response.json())
	.then(response => {

    document.getElementById(`slika1`).src = response.response.images[1].image.url;
    document.getElementById(`slika2`).src = response.response.images[2].image.url;
    document.getElementById(`slika3`).src = response.response.images[3].image.url;
    document.getElementById(`slika4`).src = response.response.images[4].image.url;
    document.getElementById(`slika5`).src = response.response.images[5].image.url;
    document.getElementById(`slika6`).src = response.response.images[6].image.url;
    document.getElementById(`slika7`).src = response.response.images[7].image.url;
    document.getElementById(`slika8`).src = response.response.images[8].image.url;
    document.getElementById(`slika9`).src = response.response.images[9].image.url;
    document.getElementById(`slika10`).src = response.response.images[10].image.url;

  })
	.catch(err => console.error(err));
  }
</script>


</html>