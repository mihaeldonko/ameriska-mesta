
<footer class="page-footer font-small bg-dark mt-3">
  <div class="footer-copyright text-center py-3 text-light">© 2022 Copyright:
    <a href="/" class="text-light"> States.com / Mihael Donko</a>
  </div>
</footer>
<script>
  function header_diff(){
    var userCheck= " <?php echo Auth::check();?>";
    if(userCheck == 1){
      document.getElementById("head_login").style.display = 'none'; 
    }else{
      document.getElementById("head_logout").style.display = 'none'; 
      document.getElementById("head_city").style.display = 'none'; 
    }
  }
  function save(){
  localStorage.clear();
  if(document.getElementById("togBtn").checked){
    localStorage.setItem("jezik", "Slovenija");
  }else{
    localStorage.setItem("jezik", "Anglija");
  }
}

function jezik_save(){
  if(localStorage.getItem("jezik") === "Slovenija"){
    document.getElementById("togBtn").checked = true;
}  else{
  document.getElementById("togBtn").checked = false;	
}
  

}
</script>
