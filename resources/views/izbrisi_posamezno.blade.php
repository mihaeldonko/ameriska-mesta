
@include('head')
<body onload="header_diff()">
@include('header')
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container mb-5">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light">Delete city city</h4>

              <h3 id="ime_states" class="mb-5  text-light">{{$mesta->city}}</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2>Agreement of deletion</h2>
                    <p>    NON-DISCLOSURE AGREEMENTS
                    A non-disclosure agreement, also known as a confidentiality agreement, details what information should not be shared outside of the parties in the agreement. It may cover information or materials (such as photos) that cannot be shared with third parties.

                    An example of a non-disclosure agreement is a HIPAA confidentiality agreement. HIPAA laws mandate that patient information cannot be shared with third parties by a patient’s healthcare provider. Many medical offices will have these non-disclosure agreements for vendors, contractors, students, or other non-employees who work with them.

                    Confidentiality agreements may be used in many other cases, as well. If you are catering a celebrity event, for example, you might need a non-disclosure agreement stating that employees may not take or share photos or audio. Our non-disclosure agreement templates make it easy to make your own.
                    </p>
                    <div>
                            <input type="checkbox" id="check" name="check">
                            <label for="horns">I accept the agreement of deletion and take full responsibility for my actions!!!</label>
                        </div>
                    <form action="{{ url('/izbris/'.$mesta->ID) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" onclick="opozorilo()">Delete city</button>
                    </form>
                    
                    
                        
                    
            </div>
        </div>
    </div>
    @include('footer')
    <script>

        function opozorilo(){
            let i = document.getElementById("ime_states").innerHTML;
            window.alert("Izbrisali ste mesto "+ i)
        }
    </script>
</body>
</html>