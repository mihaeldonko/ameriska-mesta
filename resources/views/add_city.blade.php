@include('head')
<body onload="header_diff()">
@include('header')
<body>
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container mb-5">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light">Add cities</h4>

              <h3 id="ime_states" class="mb-5  text-light">in the form below</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <form action="{{route('dodaj_mesto')}}" method="POST">
        {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleFormControlInput1">City name</label>
    <input type="text" class="form-control" name="city">
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Population growth</label>
    <input type="number" class="form-control"  name="growth_from_2000_to_2013" >
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Latitude</label>
    <input type="number" class="form-control"  name="Latitude">
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">longitude</label>
    <input type="number" class="form-control"  name="longitude" >
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Number of citizens</label>
    <input type="number" class="form-control"  name="population" >
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Rank</label>
    <input type="number" class="form-control"  name="rank" >
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect2">State</label>
    <select class="form-control form-select"  name="state">

<option value="none"></option>
<x-usa_list></x-usa_list>
</select>
  </div>

  <button class="btn btn-success mt-3" onclick="dodano();">Add city to the list!</button>
</form>
        </div>
      </div>
    </div>

@include('footer')
<script>
function dodano(){
  window.alert("City has been added");
}

</script>
</body>
</html>