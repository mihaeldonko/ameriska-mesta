@include('head')
<body onload="header_diff()">
@include('header')
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light">Login</h4>

              <h2 id="ime_states" class="mb-5  text-light">bellow</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="container mt-4 mb-5">
  <div class="row">
    <div class="col-md-8 offset-md-2">
    <h3 class="login-heading mb-4">Welcome back!</h3>
               <form action="{{url('post-login')}}" method="POST" id="logForm">
 
                 {{ csrf_field() }}
 
                <div class="form-label-group">
                <label for="inputEmail">Email address</label>
                  <input type="email" name="email" id="inputEmail" class="form-control mb-3" placeholder="Email address" >
                  
 
                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
 
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                  <input type="password" name="password" id="inputPassword" class="form-control mb-3" placeholder="Password">
                  
                   
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
 
                <button class="btn btn-lg btn-success btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Log in</button>
                <div class="text-center">Dont have an account?
                  <a class="small" href="{{url('registration')}}" >Sign Up</a></div>
              </form>
    </div>
  </div>
</div>
@include('footer')
</body>
</html>