
<nav class="navbar navbar-expand-lg navbar-light bg-dark ">
  <div class="container">
    <a class="navbar-brand text-white" href="http://localhost:8000/">ZDA <span class="text-danger">STATES</span></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item active">
          <a class="nav-link text-white" href="http://localhost:8000/" id="home">Home</a>
        </li>
        <li class="nav-item dropdown" id="head_city">
          <a class="nav-link dropdown-toggle text-light" href="#" role="button" data-bs-toggle="dropdown">Cities</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="http://localhost:8000/dodano" id="add_city">Add City</a></li>
            <li><a class="dropdown-item" href="http://localhost:8000/urejeno" id="edit_city">Edit/Delete City</a></li>
          </ul>
        </li>
        <li class="nav-item active mt-1" id="head_lang">
        <label class="switch">
          <input type="checkbox" id="togBtn" onclick="test();save();">
          <div class="slider round" id="test">
            <span class="on" >SLO</span>
            <span class="off">EN</span>
          </div>
          </label>
          </li>
        <li class="nav-item dropdown" id="head_login">
          <a class="nav-link dropdown-toggle text-light" href="#" role="button" data-bs-toggle="dropdown">Login</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{url('login')}}">Login</a></li>
            <li><a class="dropdown-item" href="{{url('registration')}}" id="register">Register</a></li>
          </ul>
        </li>
        <li class="nav-item active" id="head_logout">
          <a class="nav-link text-white" href="{{url('logout')}}">Logout</a>
        </li>
        
    </div>
  </div>
</nav>
