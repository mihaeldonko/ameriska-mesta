@include('head')
<body onload="loadImg();header_diff()">
@include('header')
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container mb-5">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light">Edit cities</h4>

              <h3 id="ime_states" class="mb-5  text-light">in the form below</h3>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container" id="test">
        <div class="row text-center">
            <div class="col-md-12 ">
            <h2>Search by city name</h2>
          <input class="form-control mr-sm-2 w-100 mb-3" id="myInput" type="text" onkeyup="filter_kljucna_beseda()">
        </div>
        </div>
        </div>
        <div class="container">
          <div class="row text-center">
            @foreach($cities as $key=>$city)
        <div class="col-md-4" id="id{{$key}}">
      <img height="250" width="250" src="" class=" mt-3" alt="" id="img{{$key}}">
      <h4 class="text-center" id="name{{$key}}">{{$city->city}}</h4>
      <a href="http://localhost:8000/urejaj_posamezno/{{$city->ID}}" class="link-primary">urejaj</a>
      <a href="http://localhost:8000/izbrisi_mesto/{{$city->ID}}" class="link-danger mb-3">izbrisi</a>
        </div>
        <p hidden>{{ ++$key }}</p>
        
        @endforeach
        
          </div>
        </div>
        
        <p id="count" hidden>{{$key}}</p>

        </div>
        
    </div>
    
@include('footer')

    <script>
function filter_kljucna_beseda(){

let input, filter, table, tr, td, txtValue;

input = document.getElementById("myInput");
filter = input.value.toUpperCase();

var count = document.getElementById("count").innerHTML;


for (let i = 0; i < count; i++) {
    td1 = document.getElementById(`name${i}`);
    if (td1) {
        txtValue =td1.textContent || td1.innerText ;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            document.getElementById(`id${i}`).style.display = "";
        } else {
          document.getElementById(`id${i}`).style.display = "none";
        }
    }
}

}
    </script>


    <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'},'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    
<script>  
    function loadImg() {
      const key = "28248024-1b0004ace62f794d221e735d1";
      var php_var = "<?php echo $key; ?>";

   for(let i = 0;i<php_var;i++){
    const search = document.getElementById(`name${i}`).innerHTML;
    const url = `https://pixabay.com/api/?key=${key}&q=${search}&image_type=photo`;
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
          try{
            document.getElementById(`img${i}`).src = data.hits[0].webformatURL;
          }catch(error){
            document.getElementById(`img${i}`).src = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/USA_Flag_Map.svg/2560px-USA_Flag_Map.svg.png";
          }

            
            });
    
   }
    
    



}
</script>
 
</body>
</html>