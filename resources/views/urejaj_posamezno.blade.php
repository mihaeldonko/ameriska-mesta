
@include('head')
<body onload="header_diff();jezik_save();">
@include('header')
<div class="page-heading about-heading header-text " style="background-image: url('../images/united-states-01-1920x500.jpg');">
      <div class="container mb-5">
        <div class="row">
          <div class="col-md-12 mt-5">
            <div class="text-content mb-5 mt-5">
              <h1 id="ime_mesta" class="mt-5  text-light" id="edit">Edit city</h4>

              <h3 id="ime_states" class="mb-5  text-light">{{$mesta->city}}</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
<div class="container">
    <div class="row">
        <div class="col-md-8 ">
        
        <form action="{{ url('/uredi_mesto/'.$mesta->ID) }}" method="post">
                        @csrf
                        @method('PUT')
                <div class="form-group">
                    <label for="exampleFormControlInput1" id="city_name">City name</label>
                    <input type="text" class="form-control" id="city_form" name="city" value="{{$mesta->city}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1" id="growth">Growth</label>
                    <input type="text" class="form-control" id="Growth" name="Growth" value="{{$mesta->growth_from_2000_to_2013}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1" >latitude</label>
                    <input type="text" class="form-control" id="latitude" name="latitude" value="{{$mesta->latitude}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">longitude</label>
                    <input type="text" class="form-control" id="longitude" name="longitude" value="{{$mesta->longitude}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1" id="populacija">population</label>
                    <input type="text" class="form-control" id="population" name="population" value="{{$mesta->population}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1" >rank</label>
                    <input type="text" class="form-control" id="rank" name="rank" value="{{$mesta->rank}}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1" id="state">state</label>
                    <input type="text" class="form-control" id="state" name="state" value="{{$mesta->state}}" disabled>
                </div>

                <button id="save" class="btn btn-danger mt-3 mb-3" disabled id="save_changes">Save changes</button>
            </form>
            <button id="omogoci" class="btn btn-success" onclick="omogoci()" id="enable">Enable edit</button>
        </div>
        </div>
</div>
@include('footer')
<script>


    function omogoci(){
        document.getElementById("city_form").disabled = false;
        document.getElementById("Growth").disabled = false;
        document.getElementById("latitude").disabled = false;
        document.getElementById("longitude").disabled = false;
        document.getElementById("population").disabled = false;
        document.getElementById("rank").disabled = false;
        document.getElementById("state").disabled = false;

        document.getElementById("save").disabled = false;


        var x = document.getElementById("omogoci");
        x.style.display = "none";


    }

</script>


