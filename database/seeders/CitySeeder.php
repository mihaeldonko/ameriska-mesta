<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;


class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::get('https://raw.githubusercontent.com/TheBizii/test/main/cities.json');
        $cities = json_decode($response->body());

        foreach($cities as $city){
        \DB::table('city')->insert([
        'city' => $city->city,
        'growth_from_2000_to_2013' =>$city->growth_from_2000_to_2013,
        'latitude'=> $city->latitude,
        'longitude'=> $city->longitude,
        'population'=> $city->population,
        'rank'=> $city->rank,
        'state'=> $city->state,
    ]);
}
    }
}
