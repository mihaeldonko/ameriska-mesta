<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\city_controller;
use App\Http\Controllers\AuthController;



Route::get('/',  [city_controller::class, 'cities']);

Route::get('/dodano',  [city_controller::class, 'dodajaj']);

Route::get('/urejeno',  [city_controller::class, 'urejaj']);

Route::get('/urejaj_posamezno/{id}',  [city_controller::class, 'urejaj_posamezno']);

Route::get('/mesto/{id}', [city_controller::class, 'mesto']);

Route::post('/dodaj_mesto', [city_controller::class, 'dodaj'])->name('dodaj_mesto');

Route::put('/uredi_mesto/{id}', [city_controller::class, 'uredi']);

Route::get('/izbrisi_mesto/{id}', [city_controller::class, 'izbrisi']);

Route::delete('/izbris/{id}', [city_controller::class, 'izbris']);

Route::get('/login', [AuthController::class, 'index']);

Route::post('/post-login', [AuthController::class, 'postLogin']); 

Route::get('/registration', [AuthController::class, 'registration']);

Route::post('/post-registration', [AuthController::class, 'postRegistration']); 

Route::get('/dashboard', [AuthController::class, 'dashboard']); 

Route::get('/logout', [AuthController::class, 'logout']);


