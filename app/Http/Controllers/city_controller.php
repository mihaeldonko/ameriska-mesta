<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;

use App\Models\city;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;



class city_controller extends Controller
{
    function cities(){
        
        $response = city::all();

        return view('testview',['cities' => $response]);
    }

    function izbrisi($id){
        if(Auth::check()){
            $cachedMesta = Redis::get('ID' . $id);
            if(isset($cachedMesta)) {
                $Mesta = city::find($id);
                return view('izbrisi_posamezno', ['mesta' => $Mesta]);
            }else {
                $Mesta = city::find($id);
                Redis::set('rank' . $id, $Mesta);
                return view('izbrisi_posamezno', ['mesta' => $Mesta]);
            }
          }
           return Redirect::to("login")->withSuccess('Opps! You do not have access');
    
        
    }

    function dodajaj(){
        if(Auth::check()){
            return view('add_city');
          }
           return Redirect::to("login")->withSuccess('Opps! You do not have access');
        
    }


    function urejaj_posamezno($id){

        $cachedMesta = Redis::get('ID' . $id);
        if(Auth::check()){
            if(isset($cachedMesta)) {
                $Mesta = city::find($id);
                return view('urejaj_posamezno', ['mesta' => $Mesta]);
            }else {
                $Mesta = city::find($id);
                Redis::set('rank' . $id, $Mesta);
                return view('urejaj_posamezno', ['mesta' => $Mesta]);
            }
          }
           return Redirect::to("login")->withSuccess('Opps! You do not have access');

    }


    function urejaj(){
        
        $response = city::all();
        if(Auth::check()){
            return view('edit_city',['cities' => $response]);
          }
           return Redirect::to("login")->withSuccess('Opps! You do not have access');
        
        
    }


    function dodaj(Request $request){
        $data = $request->all();
        $city = city::create($data);
        return redirect('http://localhost:8000/');
    }

    function uredi(Request $request, $id){
            $city = city::find($id);
            $city->city = $request->input('city');
            $city->growth_from_2000_to_2013 = $request->input('Growth');
            $city->latitude = $request->input('latitude');
            $city->longitude = $request->input('longitude');
            $city->population = $request->input('population');
            $city->rank = $request->input('rank');
            $city->state = $request->input('state');
            $city->save();
            return redirect('http://localhost:8000/urejeno');
    }


    function izbris($id){
        
        $city = city::find($id);
        $city->delete();
        return redirect('http://localhost:8000/urejeno');
    }


    function mesto($id)
    {
        $cachedMesta = Redis::get('rank' . $id);
        if(isset($cachedMesta)) {
            $Mesta = city::find($id);
            return view('posamezno_mesto', ['mesta' => $Mesta]);
        }else {
            $Mesta = city::find($id);
            Redis::set('rank' . $id, $Mesta);
            return view('posamezno_mesto', ['mesta' => $Mesta]);
        }
      }
    }

