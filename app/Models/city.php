<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class city extends Model
{
    use HasFactory;

    protected $primaryKey = 'ID';

    protected $table = 'city';
    protected $guarded = [];


}
